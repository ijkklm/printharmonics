[Spherical harmonics](https://en.wikipedia.org/wiki/Spherical_harmonics) are mathematical functions,
which are defined on the surface of a sphere.
They play an important role in solving differntial equations.
For example, they are used to solve the Hydrogen problem in physics.
This Blender python script modulates the real part of the value of a spherical harmonic 
on the surface of a sphere.
You can export, slice and print the resulting objects.
They can be used to display the analytic properties of spherical harmonics,
but they can also be used to decorate a christmas tree :). 


# How to run the script

You will need a Blender installation of version 2.8 or greater.
Get it at the project's [home page](https://www.blender.org/).
Then, you can go to Blender's scipting view and paste the script there.
You can not run the script yet, because scipy is not installed for Blender's python interpreter.
Execute the following steps to install pip (a python package manager) and then scipy on a linux-based system:

  - Open a terminal and navigate to 'path/to/blender/blender-version/python/bin/' where 'blender-version'
    is the numeric version identifier of your installation
  - Run 'curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py' to download a pip installer (see [here](https://pip.pypa.io/en/stable/installing/) for reference)
  - Run './python3.7m get-pip.py' to install pip (you might have a later python version than 3.7)
  - Run './pip3 install scipy' to install scipy

Now you sould be able to run the script in Blender.
To configure the script, take a look at the descriptions of the variables at the beginning.